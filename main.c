/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.15
        Device            :  PIC18F25K80
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "mcc_generated_files/mcc.h"
unsigned char read_flag;
uCAN_MSG *tempCanMsg;

// Set the baud rate to 125 kb/s
//    BRGCON1bits.SJW = 1; // Synchronization jump width time = 2 x TQ
//    BRGCON2bits.SAM = 1; // Bus line is sampled three times prior to the sample point
//    BRGCON2bits.SEG2PHTS = 1; // Seg2 lenght freely programmable
//    BRGCON2bits.PRSEG = 2; // Propagation time = 3 x TQ
//    BRGCON2bits.SEG1PH = 7; // Phase Segment 1 time = 8 x TQ
//    BRGCON3bits.SEG2PH = 3; // Phase Segment 2 time = 4 x TQ
//    BRGCON1bits.BRP = 3; // TQ = (2 x 4)/FOSC > 500 ns

/*
                         Main application
 */
unsigned short dt; 
/*********************************************************************
*
*                        Function Prototypes 
*
*********************************************************************/
void InitDevice(void);

/*********************************************************************
*
*                            Global Variables 
*
*********************************************************************/
unsigned int heartbeatCount;
unsigned char buttonWasPressed;

void main(void)
{
    read_flag = 0;
    // Initialize the device
     InitDevice();
    SYSTEM_Initialize();
   
    
    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable high priority global interrupts
    //INTERRUPT_GlobalInterruptHighEnable();

    // Enable low priority global interrupts.
    //INTERRUPT_GlobalInterruptLowEnable();

    // Disable high priority global interrupts
    //INTERRUPT_GlobalInterruptHighDisable();

    // Disable low priority global interrupts.
    //INTERRUPT_GlobalInterruptLowDisable();

    // Enable the Global Interrupts
    //INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
    unsigned char temp = 30;
    //tempCanMsg->frame.id = 2;
    //tempCanMsg->frame.data0 = temp;

    while (1)
    {
        dt = 0;
        IO_RA0_Toggle();
        Delay10KTCYx(500);
        ECAN_Transmit();
        // Add your application code
        //CAN_transmit(tempCanMsg);
        //while (!dt) dt = CAN_receive(tempCanMsg);
        Delay10KTCYx(250);
    }
}


/*********************************************************************
*
*                       Initialize the Device 
*
*********************************************************************/
void InitDevice(void)
{
    // Set the internal oscillator to 64MHz
    //OSCCONbits.IRCF = 7;
    //OSCTUNEbits.PLLEN = 1;
    
    // Initialize global variables to 0
    heartbeatCount = 0;
    buttonWasPressed = 0;
    
    // Initialize I/O to be digital, with PORTD (LEDs) as outputs and PORTB as inputs (pushbutton)
//    ANCON0 = ANCON1 = 0x00;
//    LATB = 0x00;
//    TRISB = 0x00;
//    TRISC = 0b00001111;  //Set RC7--RC4 as outputs
//    LATC = 0b10001111;   //Turn on LEDs D2--D0 0=on, 1=0ff
//    RB3 = 1;
//    TRISA = 0xFF;
    
    // Initialize CAN module
    InitECAN();
}
/**
 End of File
*/